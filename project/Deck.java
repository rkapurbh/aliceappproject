import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author RahulKapur
 */

/* The deck consists of an arraylist of 52 cards.
It also returns an unused card when getCard() is called*/
public class Deck {
    private ArrayList<Card> deck;
    private boolean[] dealtCards;
    
    Deck(ArrayList<Card> deck) {
        this.deck = deck;
        dealtCards = new boolean[52];
        
    }
    
    Card getCard() {
        
        Random random = new Random();
        int listIndex = random.nextInt(52);
        boolean loop = checkIfDealt(listIndex);
        while(loop) {
            listIndex = random.nextInt(52);
            loop = checkIfDealt(listIndex);
        }
        
        Card card = deck.get(listIndex);
        dealtCards[listIndex] = true;
        return card;
         
        
    }
    
    boolean checkIfDealt(int index){
        if (dealtCards[index] == false) {
            return false;
        } else {
            return true;
        }
    }
    
    
    
    
    
    
}

