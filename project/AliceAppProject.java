import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author RahulKapur
 */
public class AliceAppProject {
    private static int userValue = 0; // stores the total sum of the user's cards
    private static Deck deck; // instance of deck
    private static boolean loop; 
    private static boolean outerLoop;
    private static boolean surrender;
    private static Dealer dealer;
    private static Player player;
    private static int cash = 100; // initial amount of cash

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        
        displayOptions(); 
        outerLoop = true;
        while (outerLoop) {// loop to continue the game
            System.out.println("Let the game begin: ");
            deck = shuffleDeck();// create deck
            dealer = new Dealer(deck); // instantiate dealer
            dealCards(0);// deal first two cards to player
            dealCards(1);// deal first two cards to dealer
            displayInfo(dealer);//print dealer's hand
            displayInfo(player);// print player's hand
            loop = true;// loop for player to input commands
            surrender = false;// set surrender to false
            while(loop) {
                playerMove(player);//let the player input a move
                displayInfo(player);// display player's current hand
                //continue loop as long as player wishes
            }
            dealerMove(dealer);//method to play dealer's turn
            displayInfo(dealer);// print dealer's hand after bot is done
            evaluate(dealer, player);// evaluate the game
            cash = player.getCash();//set the cash variable to the player's new amount of cash
            System.out.println("Your cash total is: " + Integer.toString(player.getCash()));
            System.out.println("Would you like to play again: Y or N");
            Scanner scanner = new Scanner(System.in);// ask user if he/she would like to play again
            String line = scanner.nextLine();
            if (line.equalsIgnoreCase("Y")) {
                outerLoop = true;
            } else {
                outerLoop = false;//end game
            }
        
            if (player.getCash() < 10) {// check if cash is too low to continue
                outerLoop = false;
                System.out.println("You cannot enter with less than 10 dollars");
            }
        }
        
        
    }
    /* This function creates a standard deck with 52 cards*/
    private static Deck shuffleDeck() {
        ArrayList<Card> cards = new ArrayList<Card>();
        ArrayList<String> suits = new ArrayList<String>();//array list of suits
        suits.add("Club");
        suits.add("Spade");
        suits.add("Diamond");
        suits.add("Hearts");
        for (int i = 0; i < suits.size(); i++) {
            for (int j = 0; j < 13; j++) {
                Card card = new Card(j+1, suits.get(i));
                cards.add(card);// populate arraylist of cards
            }
        }
        
        return (new Deck(cards));//return new deck
    }
    
    /* This function deals the first two cards to the player and dealer*/
    private static void dealCards(int i) {// the integer distinguishes between player and dealer
        ArrayList<Card> playerCards = new ArrayList<Card>();
        playerCards.add(dealer.dealCard());
        playerCards.add(dealer.dealCard());
        Hand playerHand = new Hand(10, playerCards);
        ArrayList<Hand> hands = new ArrayList<Hand>();
        hands.add(playerHand);
        if (i == 0) {
            player = new Player(cash, hands);// instantiate player
        } else {
            playerCards.get(0).setHidden();// hide dealer's first card
            dealer.setHand(hands);// set the dealer's current hand
        }
        
        
    }
    /* This function displays the hand the person is currently holding*/
    private static void displayInfo(Person person){
        ArrayList<Hand> hands = new ArrayList<Hand>();
        if (person.getType() == 0) {// check type of person
            System.out.print("The Players Cards are: ");// type: Player
            hands = ((Player) person).getHand();
        } else {
            System.out.print("The Dealers Cards are: ");// type: Dealer
            hands = ((Dealer) person).getHand();
        }
        
        for (int k = 0; k < hands.size(); k++) {//loop through the hands a player is holding
            System.out.println();
            if (hands.size() == 1) {//Check to see if player is holding multiple hands
                System.out.print("Hand: ");
            } else {
                System.out.print("Hand " + Integer.toString(k+1) + ": ");
            }
            ArrayList<Card> cardsInHand = hands.get(k).getCards();//get cards in the hand
            for (int l = 0; l < cardsInHand.size(); l++) {
                if (l > 0) {
                    System.out.print("\t");
                }
                if (cardsInHand.get(l).isHidden()) {
                if (cardsInHand.get(l).getNumber() == 1) {// convert 1 to A
                    System.out.print("A " + cardsInHand.get(l).getSuit() + " ");
                } else if (cardsInHand.get(l).getNumber() == 11) {// convert 11 to J
                    System.out.print("J " + cardsInHand.get(l).getSuit() + " ");
                } else if (cardsInHand.get(l).getNumber() == 12) {// convert 12 to Q
                    System.out.print("Q " + cardsInHand.get(l).getSuit() + " ");
                } else if (cardsInHand.get(l).getNumber() == 13) {// convert 13 to K
                    System.out.print("K " + cardsInHand.get(l).getSuit() + " ");
                } else {
                    System.out.print(cardsInHand.get(l).getNumber() + " " + cardsInHand.get(l).getSuit() + " ");
                }
            } else {// this is for the dealer's first card
                System.out.print("Hidden Card");
            }
                
            }
        }
        
        System.out.println();
         
                
    }
    /* This function deals with the user input. It holds the logic 
    behind the five main functions I have implemented: Hit, Stand, Split, Surrender
    , and Double Down
    */
    private static void playerMove(Player player) {
        System.out.println("Pick a move");
        Scanner scanner = new Scanner(System.in);
        String line = scanner.nextLine();
        if (line.equalsIgnoreCase("Hit")) {// if player chooses to hit
            if (player.getHand().size() == 1) {// check to see if player has not split the deck
                Hand hand = player.getHand().get(0);
                if (hand.isBust() == false) {// check to see if hand has already busted
                    hand.addCard(dealer.dealCard());
                } else {
                    System.out.println("This hand is already bust");
                }
            } else {// player has split the deck
                System.out.println("Which hand would you like to hit 1 or 2");//ask user which hand he would like to hit
                Scanner secondScanner = new Scanner(System.in);
                int number = secondScanner.nextInt();
                if (number == 1) {
                    if (player.getHand().get(0).isBust() == false) {// check to see if the hand is bust
                        player.getHand().get(0).addCard(dealer.dealCard());
                    } else {
                        System.out.println("This hand is already bust");
                    }
                    
                } else {
                    if (player.getHand().get(1).isBust() == false) {// check to see if the hand is bust
                        player.getHand().get(1).addCard(dealer.dealCard());
                    } else {
                        System.out.println("This hand is already bust");
                    }
                }
                
            }
            
        } else if (line.equalsIgnoreCase("Stand")) {// if player chooses stand then break the inner while loop in the main function 
            loop = false;
            
        } else if (line.equalsIgnoreCase("Split")) {
            // if player chooses split then split current hand into two separate hands and deal a new card to each new hand
            if (player.getHand().get(0).getCards().get(0).getNumber() == player.getHand().get(0).getCards().get(1).getNumber()) {
                Hand hand = player.getHand().get(0);
                ArrayList<Card> firstCardList = new ArrayList<Card>();
                ArrayList<Card> secondCardList = new ArrayList<Card>();
                firstCardList.add(hand.getCards().get(0));
                firstCardList.add(dealer.dealCard());
                secondCardList.add(hand.getCards().get(1));
                secondCardList.add(dealer.dealCard());
                int bet = hand.getBet();
                Hand firstHand = new Hand(bet,firstCardList);// create new hand with same initial bet
                Hand secondHand = new Hand(bet,secondCardList);// create another new hand with same initial bet
                ArrayList<Hand> hands = new ArrayList<Hand>();
                hands.add(firstHand);
                hands.add(secondHand);
                player.setHand(hands);
            } else {// if card values are not equal then the player cannot split
                System.out.println("You cannot split this hand as the cards do no have the same value");
            }
            
        } else if (line.equalsIgnoreCase("Double Down")) {
            if (player.getHand().get(0).isBust() == false) {// check to see if hand is bust
                player.getHand().get(0).addCard(dealer.dealCard());//deal one final card to the player
                int bet = player.getHand().get(0).getBet();
                player.getHand().get(0).setBet(bet + bet);// double the player's bet
            } 
            
            loop = false;// break while loop in the main function
            
        }else if (line.equalsIgnoreCase("Surrender")) {
            if ((player.getHand().size() == 1) && (player.getHand().get(0).getCards().size() == 2)) {
                loop = false;
                surrender = true;// set surrender boolean to true
            } else {
                System.out.println("You cannot surrender");
            }
            
        } else {
            
        }
                
        
    }
    /* This function generates the actons of the dealer when it is the dealers turn in the game.
       The function keeps adding a card to the dealer's hand till a minimum count of 17 is reached    
    */
    private static void dealerMove(Dealer dealer) {
        dealer.getHand().get(0).getCards().get(0).setNotHidden();
        while (dealer.getHand().get(0).getTotalCount() < 17) {//loops while hand value is less than 17
            dealer.getHand().get(0).addCard(dealer.dealCard());
        }
        
        
    }
    /* This function is called once the dealer and player have made their moves it  
        computes the winner of the game and changes the amount of cash held by the user by the appropriate amount.
        If the user lost, he looses all of his bet. If he has a blackjack then he recieves
        half of his initial bet as reward and if he wins without a blackjack then he recieves the value of his bet in return
    */
    
    
    private static void evaluate(Dealer dealer, Player player) {
        // evaluates the end result of the game
        Hand dealerHand = dealer.getHand().get(0);
        ArrayList<Hand> playerHands = player.getHand();
        if (surrender == false) {// check to see if the user surrendered
            for (int i = 0; i < playerHands.size(); i++) {//loop through player's hands incase of a split
            Hand playerHand = playerHands.get(i);
            if (playerHand.isBust() == false && dealerHand.isBust() == true) {// player is not bust but dealer is
                int bet = playerHand.getBet();
                int cash = player.getCash();
                player.setCash(cash + bet);// player wins the amount of their initial bet
                System.out.println("You earned " + Integer.toString(bet));
            } else if (playerHand.isBust() == false && dealerHand.isBust() == false) {//player and dealer are not bust
                checkForHighestValue(playerHand.getCards(), 0, 0); // compute user's intended value accounting for A as either 1 or 11
                int total = userValue; 
                userValue = 0;
                if (total > dealerHand.getTotalCount()) {// user won
                    if (total == 21 && playerHand.getCards().size() == 2) {
                        int bet = playerHand.getBet();
                        int cash = player.getCash();
                        System.out.println("You earned " + Integer.toString(bet/2));
                        player.setCash(cash + (bet/2));
                    } else {
                        int bet = playerHand.getBet();
                        int cash = player.getCash();
                        System.out.println("You earned " + Integer.toString(bet));
                        player.setCash(cash + bet);// player gains the value of his bet
                    }
                } else if (dealerHand.getTotalCount() > total){// dealer won
                    int bet = playerHand.getBet();
                    int cash = player.getCash();
                    System.out.println("You lost " + Integer.toString(bet));
                    player.setCash(cash - bet);// player looses his bet
                    
                } else {// tie, i.e. no change in player's bank account
                    
                }
                
                
            } else if (playerHand.isBust() == true && dealerHand.isBust() == false) {// if player is bust and dealer is not
                int bet = playerHand.getBet();
                int cash = player.getCash();
                player.setCash(cash - bet);// take user's bet away from their cash
                System.out.println("You lost " + Integer.toString(bet));
            } else {
                
            }
            }
        } else {
            surrender = false;
        }
        
    }
    /* This function computes the user's total by use of recursion. The purpose of the recursion was to 
        was to distinguish between an A as either a 1 or 11. Despite multiple attempts, the function uses the value
        of 11 for A when computing the highest value.
    */
    
    private static void checkForHighestValue(ArrayList<Card> cards, int counter, int total){
        if (counter == cards.size() ) {
            if (total <= 21) {
                if (total > userValue) {//base case
                    userValue = total;
                    return;
                } else {
                    return;
                }
            }
            
        } else if (counter < cards.size()) {
            if (cards.get(counter).getNumber() == 1) {
                checkForHighestValue( cards, counter + 1, total + 1);//recurse A as 1
                checkForHighestValue( cards, counter + 1, total + 11);//recurse A as 11
            } else {
                if (cards.get(counter).getNumber() == 11 || cards.get(counter).getNumber() == 12 || cards.get(counter).getNumber() == 13) {
                    checkForHighestValue( cards, counter + 1, total + 10);
                } else {
                    checkForHighestValue( cards, counter + 1, total + cards.get(counter).getNumber());
                }
                
            }
        } else {
            return;
        }
        
        
        return;
        
    }
    /* Prints the list of commands that can be chosen by the player*/ 
    private static void displayOptions() {//print commands
        System.out.println("\nThese are the follwoing commands of the game: ");
        System.out.println("1) Hit \n2) Stand \n3) Split \n4) Double Down \n5) Surrender \n");
    }
    
}

