/**
 *
 * @author RahulKapur
 */

/* This class is used to define a card with a suit and number. I also 
    added a field to show whether the card is hidden or not
*/
public class Card {
    private  int number;
    private  String suit;
    private  int hidden;
    
    Card(int number , String suit) {
        this(number, suit, 0);
    }
    
    Card(int number , String suit, int hidden) {
        this.number = number;
        this.suit = suit;
        this.hidden = hidden;
    }
    
    int getNumber() {
        return this.number;
    }
    
    String getSuit() {
        return this.suit;
    }
    
    boolean isHidden() {
        if (this.hidden == 0) {
            return true;
        } else {
            return false;
        }
    }
    
    Card setNotHidden() {
        this.hidden = 0;
        return this;
    }
    
    Card setHidden() {
        this.hidden = 1;
        return this;
    }
    
    
}


