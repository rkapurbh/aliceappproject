import java.util.ArrayList;

/**
 *
 * @author RahulKapur
 */

/* The player class has a refernce to its current hand and its cash value*/
public class Player extends Person{
    
    private int totalCash;
    private ArrayList<Hand> hands;
    
    Player(int totalCash, ArrayList<Hand> hands) {
        this.hands = hands;
        this.totalCash = totalCash;
        this.type = 0;
    }
    
    int getCash() {
        return this.totalCash;
    }
    
    void setCash(int cash) {
        this.totalCash = cash;
    }
    
    ArrayList<Hand> getHand() {
        return this.hands;
    }
    
   
    
    void setHand(ArrayList<Hand> hand) {
        this.hands = hand;
    }
    
   
    
    
    
}

