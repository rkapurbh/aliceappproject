import java.util.ArrayList;

/**
 *
 * @author RahulKapur
 */

/* The hand class contains an arraylist of cards currently held by a person. It also 
    dynamically computes the total value of the hand as cards are added. I implemented a 
    recursives function to obtain the max value of the hand while accounting for A as either 1 or 11.
    This class also keeps track of the bet that is attributed to each hand
    
*/
public class Hand {
    private int totalCount = 0;
    private int bet;
    private ArrayList<Card> cards;
    private boolean bust;
    
    Hand(int bet, ArrayList<Card> cards) {
        this.bet = bet;
        this.cards = cards;
        this.bust = false;
        computeTotal(this.cards, 0, 0);
    }
    
    int getBet() {
        return this.bet;
    }
    
    void setBet(int bet) {
        this.bet = bet;
    }
    
    ArrayList<Card> getCards(){
        return this.cards;
    }

    void addCard(Card card) {
        this.cards.add(card);
        computeTotal(this.cards, 0, 0);
        
        if (totalCount > 21) {
            setBust();
        }
        
    }
    
    int getTotalCount() {
        return this.totalCount;
    }
    
    void setBust() {
        this.bust = true;
    }
    
    boolean isBust() {
        return this.bust;
    }
    
    void computeTotal(ArrayList<Card> cards, int counter, int total){
        
       
        if (counter == cards.size() ) {
                if (total > totalCount) {
                    totalCount = total;
                } else {
                    return;
                }
           
            
        } else if (counter < cards.size()) {
            if (cards.get(counter).getNumber() == 1) {
                computeTotal( cards, counter + 1, total + 1);
                computeTotal( cards, counter + 1, total + 11);
            } else {
                if (cards.get(counter).getNumber() == 11 || cards.get(counter).getNumber() == 12 || cards.get(counter).getNumber() == 13) {
                    computeTotal( cards, counter + 1, total + 10);
                } else {
                    computeTotal( cards, counter + 1, total + cards.get(counter).getNumber());
                }
                
            }
        } else {
            return;
        }
        
        return;
        
    }
    
    
    
}

