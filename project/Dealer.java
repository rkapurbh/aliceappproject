import java.util.ArrayList;

/**
 *
 * @author RahulKapur
 */

/* The dealer class is a type of person with int value 1, it has access to its hand and deck*/
public class Dealer extends Person{
    private Deck deck;
    private ArrayList<Hand> hands;
    
    Dealer(Deck deck) {
        this.deck = deck;
        this.type = 1;
        hands = new ArrayList<Hand>();
    }
    
    Card dealCard() {
        return this.deck.getCard();
    }
    
    ArrayList<Hand> getHand() {
        return this.hands;
    }
    
    
    
    void setHand(ArrayList<Hand> hand) {
        this.hands = hand;
    }
    
    
    
    

   
    
}

