Notes on Project:
Everything works as it should, the code is also well commented. One particular issue I was running into was determining which value to use for A (1 or 11) when determining the value of a hand. I have attempted to solve that by writing a recursive function that computes all possible values of a given hand, and then choosing the most appropriate value for both the player and dealer within the function itself. Unfortunately the function does not work as it should and due to time constraints I was not able to rectify that issue, so as far as my implementation were to go an A would represent 11 and not a 1. Though I do believe my recursive implementation to be close to what the appropriate function would be. Other than this issue the rest of the code works well and implements OO principles as requested. If there are any issues please do reach out to me and hopefully I could aid in rectifying them.


To Run:


- make


- java AliceAppProject

Rahul Kapur
rk2749@columbia.edu